Meteor.subscribe('rooms');
Meteor.subscribe('allUsersData');
Meteor.subscribe('allButYouUsersData');

Meteor.startup(function() {
	Tracker.autorun(function(){
		var room = Session.get('curr_room');
		if (room) {
			Meteor.subscribe('roomMessages', room);
		}
	});
});


Template.body.helpers({
	curr_messages: function() {
		return Messages.find();
	},

	users: function() {
		if(Meteor.users.findOne()) {
			return AllButYouUsers.find();
		} else {
			return Users.find();
		}
	},

	rooms: function() {
		return Rooms.find();
	},

	curr_room: function() {
		return Session.get('curr_room');
	}

});

Template.roomItem.helpers({
	is_current: function () {
		return this.name == Session.get('curr_room');
	}
});

//does it need to be here in this section?
Messages.find().observe({
	added: function(message) {
		//scroll to last message
		$(".messages").animate({
			scrollTop: $("#messages")[0].scrollHeight
		});
	}
});

Rooms.find().observe({
	added: function (room) {
		if(!Session.get('curr_room')) {
			Session.set('curr_room', room.name);
		}
	}
});


Template.body.events({
	'submit form': function(e) {
		e.preventDefault();

		var message = {
			user: Meteor.users.findOne().username,
			message: $(e.target).find('[name=message]').val(),
			room: Session.get('curr_room')
		};

		message._id = Messages.insert(message);

		$(e.target).find('[name=message]').val('');
	},

	'click .username': function(e) {
		//I know about multiple clicks, but target of this app for me is to learn basics of MeteorJS framework =)
		$(document).find('input[name=message]').val(this.username + ': ' + $(document).find('input[name=message]').val());
	},

	'click .roomname': function(e) {
		Session.set('curr_room', this.name);
	}
});


