Meteor.publish('roomMessages', function(room) {

	if(!room) {
		room = Rooms.findOne().name;
	}

 	return Messages.find({room: room});
});