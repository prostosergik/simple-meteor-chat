Meteor.publish('allUsersData', function() {
  var self, users;
  self = this;
  users = Meteor.users.find().map(function(user) {
    ret = {
      _id: user._id,
      username: user.username
    };
    self.added('allUsersData', ret._id, ret);
    return ret;
  });
  self.ready();
});

//just ty try how publishing works =)
Meteor.publish('allButYouUsersData', function() {
  var self, users;
  self = this;
  users = Meteor.users.find().map(function(user) {
  	if (user._id != self.userId) {
	    ret = {
	      _id: user._id,
	      username: user.username
	    };
	    self.added('allButYouUsersData', ret._id, ret);
	    return ret;
	}
  });
  self.ready();
});